package com.example.demo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationDemo {
	public static void main(String[] args) throws IOException {
		Add add= new Add();
		add.setData(100,200);
		add.cal();
		FileOutputStream stream=new FileOutputStream("file.dat");
		ObjectOutputStream out=new ObjectOutputStream(stream);
		out.writeObject(add);
		out.close();
		System.out.println("File is serialised");
	}

}
