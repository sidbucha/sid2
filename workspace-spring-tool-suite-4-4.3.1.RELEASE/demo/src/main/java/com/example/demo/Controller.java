package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDAO ob;
@GetMapping("/hello")
public String display() {
	return "Welcome to ms";
}
@GetMapping("/hello1")
public Student display1(){
	return new Student("Ramu", 14,"kashmir");
}
@GetMapping("/students")
public List<Student> getStudents(){
	return ob.getStudent();
}
@GetMapping ("/student/{name}")
	public List<Student> getStudents2( @PathVariable String name){
		return ob.getDetails(name);
	}
@RequestMapping (method=RequestMethod.POST, value="/addstudent")
public String insert(@RequestBody Student stu) {
	return ob.insert(stu);
}
@DeleteMapping ("/delete/{name}")
public String deleteStudents(@PathVariable String name){
	boolean a=ob.delete(name);
	return a?"Deleted successfully":"Not deleted";
}
@PutMapping("/update/{name}")
public String updateStudents(@PathVariable String name,@RequestBody Student stu) {
	return ob.update(name,stu);
}
}
		



