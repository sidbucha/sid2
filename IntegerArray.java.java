package com.sapient.week1;

import java.util.Arrays;

public class IntegerArray {
	public int size;
	public int[] array;
	IntegerArray() {
		 array=new int[10];
	}
	IntegerArray(int size){
		this.size=size;
		 array=new int[size];
	}
	
	IntegerArray(IntegerArray i) {
		array=i.array;
	}
	
	IntegerArray(int a[]){
		array= a;
	}
	
	public void read() {
		for(int i=0;i<size;i++) {
			array[i]=Read.sc.nextInt();
		}
	}
	
	public void display() {
		for(int i=0;i<size;i++) {
			System.out.print(array[i]+" ");
		}
	}
	
	public int average() {
		int avg=0;
		for(int i=0;i<size;i++) {
			avg+=array[i];
		}
		return avg;
	}
	
	public void sort() {
		Arrays.sort(array);
	}
	
	public int search(int n) {
		int ans=Arrays.binarySearch(array, n);
		return ans;
	}
	
	public static void main(String[] args) {
		int[] array2={0,1,2,3,4,5,6,7,8,9};
		IntegerArray obj= new IntegerArray(5);
		obj.read();
		obj.display();
	}

}
