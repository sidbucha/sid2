package com.sapient.week1;
import java.util.*;
public class Controller {
	@Override
	protected void finalize() throws Throwable
    {   
        System.out.println("finalize method called"); 
        super.finalize();
    }
	public static void main(String[] args) {
		
			AddBean addbean = new AddBean();
			Runtime.getRuntime().gc();
			View view = new View();
			AddDAO adddao = new AddDAO();
			view.readData(addbean);
			adddao.compute(addbean);
			view.display(addbean);
			addbean = null;
			System.gc();
			
		
	}
	   

}



