package com.sapient.week1;

public class Matrix {
	public int m1,n1,m2,n2;
	public int mat1[][];
	public int mat2[][];
	public int mat3[][];
	Matrix(){
		m1=3;n1=3;m2=3;n2=3;
		mat1= new int[3][3];
		mat2= new int[3][3];
		mat3= new int[3][3];
	}
	Matrix(int m1,int n1,int m2,int n2){
		this.m1=m1;this.n1=n1;
		mat1= new int[m1][n1];
		
		this.m2=m2;this.n2=n2;
		mat2= new int[m2][n2];
		
		mat3= new int[m1][n2];
	}
	
	Matrix(int[][] pehla,int[][] dujja) {
		mat1=pehla;
		mat2=dujja;
	}
	
	public void ReadMat1() {
		for(int i=0;i<m1;i++) {
			for(int j=0;j<n1;j++) {
				mat1[i][j]=Read.sc.nextInt();
			}
		}
	}
	public void ReadMat2() {
		for(int i=0;i<m2;i++) {
			for(int j=0;j<n2;j++) {
				mat2[i][j]=Read.sc.nextInt();
			}
		}
	}
	
	public void addMat() {
		for(int i=0;i<m1;i++) {
			for(int j=0;j<n1;j++) {
				mat3[i][j]=mat1[i][j]+mat2[i][j];
			}
		}
	}
	
	public void subMat() {
		for(int i=0;i<m1;i++) {
			for(int j=0;j<n1;j++) {
				mat1[i][j]-=mat2[i][j];
			}
		}
	}
	
	public void mulMat() {
		if(n1==m2) {
			for(int i=0;i<m1;i++) {
				for(int j=0;j<n1;j++) {
					for(int k=0;k<n2;k++) {
						mat3[i][j]+=mat1[i][k]*mat2[k][j];
					}
				}
			}
		}
		else {
			System.out.println("Multiplication not possible");
		}
	}
	
	public void displayMat3() {
		for(int i=0;i<m1;i++) {
			for(int j=0;j<n1;j++) {
				System.out.print(mat1[i][j]+" ");
			}
			System.out.println();
		}
	}
	public void displayMat2() {
		for(int i=0;i<m2;i++) {
			for(int j=0;j<n2;j++) {
				System.out.print(mat2[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Matrix obj=new Matrix();
		obj.ReadMat1();
		obj.ReadMat2();
		//obj.addMat();
		//obj.displayMat3();
		obj.mulMat();
		obj.displayMat3();
	}

}
