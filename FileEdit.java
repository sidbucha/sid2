package com.sapient.week1;

import java.io.RandomAccessFile;
import java.io.*;
public class FileEdit {

	public static void main(String[] args) throws Exception {
		long n = 0;
		File f1;
		String fname;
		RandomAccessFile rm;
		System.out.println("Enter file name to edit");
		fname = Read.sc.nextLine();
		f1 = new File(fname);
		rm = new RandomAccessFile(f1, "rw");
		int ch;
		while((ch=rm.read()) != -1){
			if(ch == 97) {
				rm.seek(n);
				rm.write(65);
			}
			n++;
		}
		rm.close();
		System.out.println("File is edited");

	}

}
