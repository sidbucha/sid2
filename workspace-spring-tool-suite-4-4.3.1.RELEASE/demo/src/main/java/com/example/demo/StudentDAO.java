package com.example.demo;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service

public class StudentDAO {
	@Autowired
	StudentList l1;
	public List<Student> getStudent(){
		
		return l1.getList();
	}
	public List<Student> getDetails(String name) {
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	public String insert(Student ob) {
		l1.getList().add(ob);
		return "added successfully";
	}
	public boolean delete(String name) {
		return l1.getList().removeIf(ob->ob.getName().equals(name));	 
	}
	public String update(String name, Student obj) {
		for(Student ob: l1.getList())
		{
			if(ob.getName().equals(name)) {
				ob.setName(obj.getName());
				ob.setCity(obj.getCity());
				ob.setAge(obj.getAge());
				return "updated successfully";	
			}
			
			
				
		}
		return "not updated";
		
	}
	

}
