package com.sapient.week1;

class FigToWords1 {
	public static String getWords(long amt) {
		String word = "";
		String[] unit = {"","one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","ninteen"};
		String[] tens = {"","ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"};
		String[] vunit = {"crores","lakhs","thousands","hundreds","only"};
		long[] nunit = {10000000L,100000L,1000L,10L,1};
		for(int i = 0; i < nunit.length; i++) {
			int n = (int)(amt/nunit[i]);
			amt = amt%nunit[i];
			if(n>0) {
				if(n>19) {
					word += tens[n/10] + unit[n%10] + vunit[i];
				}
				else {
					word += unit[n]+vunit[i];
				}
			}
		}
		return word;
	}

}

public class FigToWords{
	public static void main(String args[]) {
		System.out.println(FigToWords1.getWords(22111));
		
	}
}
