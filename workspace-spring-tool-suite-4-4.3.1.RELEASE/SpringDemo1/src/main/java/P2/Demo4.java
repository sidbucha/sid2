package P2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo4 {
	public static void main(String[] args) {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml") ;
			Arithmetic ob;
			ob=(Arithmetic)context.getBean("arithmetic");	
		    ob.add(5,12);
		    ob.sub(1500,1200);
		    ob.mul(500,300 );
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	

}
}

