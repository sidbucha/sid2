package com.sapient.week1;
import java.util.*;
import java.util.stream.Collectors;
public class Duplicates {
	public static void main(String args[]) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(4);
		al.add(4);
		al.add(7);
		al.add(8);
		Set<Integer> lhs = new LinkedHashSet<Integer>(al);
		System.out.println(lhs);
		
		List<Integer> list = al.stream().distinct().collect(Collectors.toList());
		System.out.println(list);
	}
}
