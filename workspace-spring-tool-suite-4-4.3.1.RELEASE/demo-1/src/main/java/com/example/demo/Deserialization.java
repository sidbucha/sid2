package com.example.demo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Deserialization {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		FileInputStream stream= new FileInputStream("file.dat");
		ObjectInputStream in =new ObjectInputStream(stream);
		Add ob=(Add) in.readObject();
		in.close();
		ob.display();
	}

}
