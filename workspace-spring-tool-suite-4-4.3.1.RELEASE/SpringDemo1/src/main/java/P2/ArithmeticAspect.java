package P2;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
@Aspect
@Order(0)
public class ArithmeticAspect {


		@Before("execution(* *.*(double,double))")
		public void check1(JoinPoint j) {
			System.out.println("Logging started:");
			System.out.println("Signature of the called function is :");
			System.out.println(j.getSignature());
			for(Object x: j.getArgs())
			{
				double d= (Double)x;
				if(d<0)
					throw new IllegalArgumentException("Argument cant be negative ");    //Arguements cannot be negative
					
				String s = String.valueOf(x);
				System.out.println("Arg:"+s+"     ");
				
			}
			System.out.println("Function called on time:"+LocalDateTime.now());
			
		}
		
		@AfterReturning(pointcut="execution(* *.*(double,double))",returning="val")
		public void check2(JoinPoint j, Object val) {
			double y=(Double)val;
			if(y>1000)// 1000 is the limit of the result.
			throw new IllegalArgumentException("Result cannot exceed 1000");
			else {
				System.out.println("Result is "+y);
				System.out.println("Logging ended......");
			}
		}
}
				
		


