package com.example.demo;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {
	private List<Student> list;

	public StudentList() {
		super();
	    list = new ArrayList<>();
		list.add(new Student("Ram",21,"kk"));
		list.add(new Student("sham",21,"kk"));
		list.add(new Student("gopal",21,"kk"));
	}

	@Override
	public String toString() {
		return "StudentList [list=" + list + "]";
	}

	

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}
	
	
	

}
