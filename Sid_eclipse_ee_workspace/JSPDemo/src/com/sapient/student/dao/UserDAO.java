package com.sapient.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.bean.UserBean;

public class UserDAO {
	
	public Map<String, UserBean> getAllUsers() throws Exception{
		Connection con = DBConnection.getConnection();
		PreparedStatement ps = con.prepareStatement("Select * from users");
		Map<String,UserBean> map = new TreeMap<String, UserBean>();
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		map.put("column_name", new UserBean(meta.getColumnName(1), meta.getColumnName(2),meta.getColumnName(3), meta.getColumnName(4)));
		while(rs.next()) {
			String s1 = rs.getString(1);
			String s2 = rs.getString(2);
			String s3 = rs.getString(3);
			String s4 = rs.getString(4);
			map.put(s3, new UserBean(s1, s2, s3, s4));
		}
		return map;
	}

	public void insert(UserBean ub, StudentBean sb) throws Exception {
		Connection con = DBConnection.getConnection();
		PreparedStatement ps = con.prepareStatement("insert into users values (?,?,?,?)");
		ps.setInt(1, Integer.parseInt(ub.getId()));
		ps.setString(2, ub.getUsername());
		ps.setString(3, ub.getPass());
		ps.setString(4, ub.getType());
		ps.executeQuery();
		
		ps = con.prepareStatement("insert into students values (?,?,?,?)");
		ps.setInt(1, Integer.parseInt(sb.getId()));
		ps.setString(2, sb.getName());
		ps.setInt(3, Integer.parseInt(sb.getRollno()));
		ps.setInt(4, Integer.parseInt(sb.getPercent()));
		ps.executeQuery();
	}
	
}
