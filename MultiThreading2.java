package com.sapient.week1;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;
import java.util.*;


public class MultiThreading2 {
	

	// max 1 people
	static Semaphore semaphore = new Semaphore(1);

	static class MyLockerThread extends Thread {
		MyLockerThread(){}
		String name = "";public int sum = 0;

		public int getSum() {
			return sum;
		}

		MyLockerThread(String name) {
			this.name = name;
		}

		public void run() {

			try {

				System.out.println(name + " : acquiring lock...");
				System.out.println(name + " : available Semaphore permits now: " 
								+ semaphore.availablePermits());

				semaphore.acquire();
				System.out.println(name + " : got the permit!");
				int randomNum = ThreadLocalRandom.current().nextInt(0, 30 + 1);
				sum = sum + randomNum;
				Thread.sleep(1000);
				
				} 
			catch (InterruptedException e) {

				e.printStackTrace();

			}finally {

					// calling release() after a successful acquire()
					System.out.println(name + " : releasing lock...");
					semaphore.release();
					System.out.println(name + " : available Semaphore permits now: " 
								+ semaphore.availablePermits());

				}

		 
		}
		}



	public static void main(String[] args) {

		System.out.println("Total available Semaphore permits : " 
				+ semaphore.availablePermits());
		MyLockerThread mlt = new MyLockerThread();
		for(int i = 0; i < 5; i++) {
		MyLockerThread t1 = new MyLockerThread("A");
		t1.start();
		if(mlt.getSum() > 100) {
			System.out.println(t1.name+" wins");
			Thread.currentThread(); 
		}

		MyLockerThread t2 = new MyLockerThread("B");
		t2.start();

		MyLockerThread t3 = new MyLockerThread("C");
		t3.start();
		
		}

	}
}