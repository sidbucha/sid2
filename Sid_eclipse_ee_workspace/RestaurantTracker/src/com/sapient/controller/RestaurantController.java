package com.sapient.controller;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class RestaurantController {
	
	public static void main(String[] args) {
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
//		String response = target.path("geocode")
//				.queryParam("lat", 28.4595)
//				.queryParam("lon", 77.0266)
//				.request()
//				.header("user-key", "73bf72e931b98b31160e87d3d91651e1")
//				.accept(MediaType.APPLICATION_JSON)
//				.get(String.class);
		System.out.println(target.path("geocode")
				.queryParam("lat", 28.4595)
				.queryParam("lon", 77.0266)
				.request()
				.header("user-key", "73bf72e931b98b31160e87d3d91651e1")
				.accept(MediaType.TEXT_PLAIN)
				.get(String.class));
		
		
		
	}

	private static URI getBaseUri() {
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1").build();
	}
	
}
